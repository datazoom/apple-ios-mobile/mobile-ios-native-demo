//
//  VideoType.swift
//  DataZoomGold
//
//  Created by Milos Mokic on 31/07/2020.
//  Copyright © 2020 iOS Developer. All rights reserved.
//

import Foundation

enum VideoType {
    case mp4
    case mp4Random
    case hls
    case mpd
    case live
    case customUrl(_ url: String)
    
    static let defaultType: Self = .mp4
    
    var title: String {
        switch self {
        case .mp4:
            return "VOD / MP4"
        case .mp4Random:
            return "VOD / MP4 - Random video"
        case .hls:
            return "M3U8 / HLS"
        case .mpd:
            return "VOD / MPD"
        case .live:
            return "Live stream"
        case .customUrl:
            return "Custom Url"
        }
    }
    
    var url: String {
        switch self {
        case .mp4:
//            return MP4.getVideoUrl(at: 2)
            return "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"
      case .mp4Random:
            return MP4.getRandomVideoUrl()
        case .hls:
            //return "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8"
          return "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8"
        case .mpd:
            return "https://s3.amazonaws.com/_bc_dml/example-content/sintel_dash/sintel_vod.mpd"
        case .live:
            //return "https://dwstream4-lh.akamaihd.net/i/dwstream4_live@131329/master.m3u8"
          return "https://cph-p2p-msl.akamaized.net/hls/live/2000341/test/master.m3u8"
        case .customUrl(let value):
            return value
        }
    }
    
    var shouldRequestAd: Bool {
        switch self {
        case .mp4, .mp4Random, .hls, .mpd, .live, .customUrl:
            return true
        }
    }
    
    var shouldEnterCustomUrl: Bool {
        switch self {
        case .customUrl:
            return true
        default:
            return false
        }
    }
    
    static var allCases: [VideoType] = [.mp4, .mp4Random, .hls, .live, .customUrl("")]
//    static var allCases: [VideoType] = [.mp4, .hls, .mpd, .live]
}


fileprivate struct MP4 {
    private static var videos: [String] = [
        "http://thenewcode.com/assets/videos/zsystems-alt.mp4",
        "http://thenewcode.com/assets/videos/aotearoa.mp4",
        "http://thenewcode.com/assets/videos/atlantic-light.mp4",
        "http://thenewcode.com/assets/videos/glacier.mp4"
    ]
    
    static func getRandomVideoUrl() -> String {
        return videos.randomElement()!
    }
    
    static func getVideoUrl(at index: Int) -> String {
      return videos[index] 
    }
}
