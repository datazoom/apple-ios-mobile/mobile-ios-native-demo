//
//  AppError.swift
//  DataZoomGold
//
//  Created by Vlada Calic on 17.7.21..
//  Copyright © 2021 iOS Developer. All rights reserved.
//

import Foundation

enum AppError: Error {
  case noPlayer
}
