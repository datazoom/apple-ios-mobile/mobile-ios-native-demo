//
//  Toast.swift
//  DataZoomGold
//
//  Created by Vlada Calic on 7.9.21..
//  Copyright © 2021 iOS Developer. All rights reserved.
//

import Foundation
import Toast_Swift


extension UIViewController {
  func showToast(string: String) {
    view.makeToast(string)
  }
}
