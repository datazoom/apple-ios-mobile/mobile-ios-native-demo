//
//  TimerCode.swift
//  Datazoom Gold Collector
//
//  Created by Vlada Calic on 29/03/2021.

import UIKit

protocol TimerCodeDelegate: AnyObject {
  func perform(in timerCode: TimerCode)
}

@objc final class TimerCode: NSObject {
  weak var delegate: TimerCodeDelegate?
  
  private let interval: Int
  private var internalTimer : Timer?
  private let tag: String

  init(interval: Int, tag: String) {
    self.interval = interval
    self.tag = tag
  }
  
  private var timedCodeInterval: Double {
      return Double(interval / 1000)
  }
  

  func start() {
    addObserver()
  }
  
  func stop() {
    if internalTimer == nil { Log.error ("**** No internal timer ")}
      internalTimer?.invalidate()
      internalTimer = nil
  }
  
  deinit {
    stop()
  }
  
  private func addObserver() {
    if self.internalTimer != nil { return }

    DispatchQueue.main.async { [self] in
          self.internalTimer = Timer.scheduledTimer(
              timeInterval: timedCodeInterval,
              target: self,
              selector: #selector(fire(timer:)),
              userInfo: nil,
              repeats: true
          )
      }
  }
  
  @objc private func fire(timer: Timer) {
    delegate?.perform(in: self)
  }
}
