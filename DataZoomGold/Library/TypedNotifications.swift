//
//  TypedNotifications.swift
//  Byrccom
//
//  Created by Vlada Calic on 1/9/20.
//  Copyright © 2020 Byrccom. All rights reserved.
//

import Foundation

protocol TypedNotification {
  associatedtype Sender
  static var name: String { get }
  var sender: Sender { get }
  
  func post()
}

extension TypedNotification {
  static var notificationName: Notification.Name {
    return Notification.Name(rawValue: name)
  }
  
  func post() {
    NotificationCenter.default.post(self)
  }
  
  static func on(on queue: OperationQueue?, using block: @escaping (Self) -> Void) {
    _ = NotificationCenter.default.addObserver(Self.self, sender: nil, queue: queue, using: block)
  }
  
  static func on(using block: @escaping (Self) -> Void) {
    _ = NotificationCenter.default.addObserver(Self.self, sender: nil, queue: nil, using: block)
  }
}

protocol TypedNotificationCenter {
  func post<N: TypedNotification>(_ notification: N)
  func addObserver<N: TypedNotification>(_ forType: N.Type,
                                         sender: N.Sender?,
                                         queue: OperationQueue?,
                                         using block: @escaping (N) -> Void) -> NSObjectProtocol
}

extension NotificationCenter: TypedNotificationCenter {
  static var typedNotificationUserInfoKey = "_TypedNotification"
  
  func post<N>(_ notification: N) where N: TypedNotification {
    post(name: N.notificationName, object: notification.sender,
         userInfo: [
          NotificationCenter.typedNotificationUserInfoKey: notification
         ])
  }
  
  func addObserver<N>(_ forType: N.Type, sender: N.Sender?, queue: OperationQueue?, using block: @escaping (N) -> Void) -> NSObjectProtocol where N: TypedNotification {
    return addObserver(forName: N.notificationName, object: sender, queue: queue) { n in
      guard let typedNotification = n.userInfo?[NotificationCenter.typedNotificationUserInfoKey] as? N else {
        Log.error("Could not construct a typed notification: \(N.name) from notification: \(n)")
        return
      }
      
      block(typedNotification)
    }
  }
}
