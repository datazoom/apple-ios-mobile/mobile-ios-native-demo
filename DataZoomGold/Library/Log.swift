//
//  Log.swift
//  DataZoomGold
//
//  Created by Vlada Calic on 8.8.21..
//

import Foundation
import SwiftyBeaver

///  Interface class for logging. Internally uses SwiftyBeaver.
class Log {
  static var shared = Log()
  fileprivate let log: SwiftyBeaver.Type = SwiftyBeaver.self
  private let docsDir = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
  
  init() {
    let console = ConsoleDestination()
    console.format = "$C$L($X) [$DHH:mm:ss]: $M$c"
    log.addDestination(console)
  }
  
  private func URLPath(for file: String) -> URL {
    return URL(fileURLWithPath: docsDir.absoluteString).appendingPathComponent(file)
  }
  
  func verbose(_ message: Any, context: LoggerContext = .general) {
    log.verbose(message, context: context.value)
  }
  
  func warning(_ message: Any, context: LoggerContext = .general) {
    log.warning(message, context: context.value)
  }
  
  func info(_ message: Any, context: LoggerContext = .general) {
    log.info(message, context: context.value)
  }
  
  func debug(_ message: Any, context: LoggerContext = .general) {
    log.debug(message, context: context.value)
  }
  
  func error(_ message: Any, context: LoggerContext = .general) {
    log.error(message, context: context.value)
  }
  
  func setupLogging() {
    let console = ConsoleDestination()
    console.minLevel = .info
    log.addDestination(console)
    
    let file = FileDestination()
    file.logFileURL = URLPath(for: "app.log")
    log.addDestination(file)
    
    /*
     let httpFile = FileDestination()
     httpFile.minLevel = .error
     httpFile.logFileURL = URLPath(for: "http.log")
     httpFile.addFilter(Filters.Path.contains("NetworkClient", caseSensitive: true, required: true, minLevel: .verbose))
     log.addDestination(httpFile)
     */
    
    log.info("Logging is setup")
  }
  
  fileprivate static func dispatch(message: Any, context: LoggerContext, file: String, function: String, line: Int, level: LoggerLevel) {
    let fileName = file.split(separator: "/").last
    let finalMessage = "\(fileName!):\(function):\(line) \(message)"
    switch level {
      case .verbose:
        Log.shared.verbose(finalMessage, context: context)
      case .warning:
        Log.shared.warning(finalMessage, context: context)
      case .debug:
        Log.shared.debug(finalMessage, context: context)
      case .info:
        Log.shared.info(finalMessage, context: context)
      case .error:
        Log.shared.error(finalMessage, context: context)
    }
  }
  
  /// Class method for verbose logging
  static func verbose(_ message: Any, _ file: String = #file, _ function: String = #function, line: Int = #line) {
    Log.dispatch(message: message, context: .general, file: file, function: function, line:line, level: .verbose)
  }
  
  /// Class method for verbose logging with given context
  static func verbose(_ message: Any, context: LoggerContext, _ file: String = #file, _ function: String = #function, line: Int = #line) {
    Log.dispatch(message: message, context: context, file: file, function: function, line:line, level: .verbose)
  }
  
  /// Class method for info logging with given context
  static func info(_ message: Any, context: LoggerContext, _ file: String = #file, _ function: String = #function, line: Int = #line) {
    Log.dispatch(message: message, context: context, file: file, function: function, line:line, level: .info)
  }
  
  /// Class method for info logging with
  static func info(_ message: Any, _ file: String = #file, _ function: String = #function, line: Int = #line) {
    Log.dispatch(message: message, context: .general, file: file, function: function, line:line, level: .info)
  }
  
  /// Class method for info logging
  static func debug(_ message: Any, _ file: String = #file, _ function: String = #function, line: Int = #line) {
    Log.dispatch(message: message, context: .general, file: file, function: function, line:line, level: .debug)
  }
  
  /// Class method for debug logging with given context
  static func debug(_ message: Any, context: LoggerContext, _ file: String = #file, _ function: String = #function, line: Int = #line) {
    Log.dispatch(message: message, context: context, file: file, function: function, line:line, level: .debug)
  }
  
  /// Class method for error logging
  static func error(_ message: Any, _ file: String = #file, _ function: String = #function, line: Int = #line) {
    Log.dispatch(message: message, context: .general, file: file, function: function, line:line, level: .error)
  }
  
  /// Class method for error logging with given context
  static func error(_ message: Any, context: LoggerContext, _ file: String = #file, _ function: String = #function, line: Int = #line) {
    Log.dispatch(message: message, context: context, file: file, function: function, line:line, level: .error)
  }
  
  /// Class method for warning logging
  static func warning(_ message: Any, _ file: String = #file, _ function: String = #function, line: Int = #line) {
    Log.dispatch(message: message, context: .general, file: file, function: function, line:line, level: .warning)
  }
  
  /// Class method for warning logging with given context
  static func warning(_ message: Any, context: LoggerContext, _ file: String = #file, _ function: String = #function, line: Int = #line) {
    Log.dispatch(message: message, context: context, file: file, function: function, line:line, level: .error)
  }
}

fileprivate enum LoggerLevel {
  case verbose, warning, debug, info, error
}

/// Context for logger. Useful for filtering.
enum LoggerContext {
  case general
  case cast
  
  var value: String {
    switch self {
      case .general:
        return "DemoApp"
      case .cast:
        return "Chromecast"
    }
  }
}
