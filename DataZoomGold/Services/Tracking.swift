//
//  Tracking.swift
//  DataZoomGold
//
//  Created by Vlada Calic on 18.6.21..
//  Copyright © 2021 iOS Developer. All rights reserved.
//

import Foundation

import AdSupport
import AppTrackingTransparency

class Tracking {
  
  var canTrack = true
  var idfa: String?
  
  static let shared = Tracking()
  func askConsent() {
    if #available(iOS 14, *) {
      ATTrackingManager.requestTrackingAuthorization { status in
        DispatchQueue.main.async { [self] in
          switch status {
            case .authorized:
              idfa = ASIdentifierManager.shared().advertisingIdentifier.uuidString
            case .denied,
                 .notDetermined,
                 .restricted:
              canTrack = false
              break
            @unknown default:
              canTrack = false
              break
          }
        }
      }
    } else {
      // Fallback on earlier versions
    }
  }
}
